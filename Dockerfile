FROM centos:7
LABEL MAINTAINER="Jonas Cavalcanti <jonascavalcantineto@gmail.com>"

# Install packages necessary to run EAP
RUN yum update -y && yum -y \ 
						install \
						java-1.8.0-openjdk-devel.x86_64 \
						python-setuptools \
						maven \
						&& yum clean all

RUN easy_install supervisor

ADD confs/supervisord.conf /etc/supervisord.conf
ADD application /opt/application

ENV APP_PATH="/opt/application"
ENV JAR_FILE="spring-rest-hello-world-1.0.jar"

RUN set -ex \
		&& cd ${APP_PATH} \
		&& mvn clean install

# Start Application Script
ADD confs/start-app.sh /start-app.sh
RUN chmod +x /start-app.sh

# Start Supervisord
ADD confs/start.sh /start.sh
RUN chmod +x /start.sh

# Start Supervisord
CMD ["/start.sh"]

