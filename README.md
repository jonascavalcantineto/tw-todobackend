# tw-todobackend

# Spring REST Hello World Example

Article link : https://www.mkyong.com/spring-boot/spring-rest-hello-world-example/

## 1. How to start
```
$ docker-compose up -d


$ curl -v localhost:8080/books
```

## Deploy on Kubernetes file tw-todobackend-kube.yml

[Link do projeto com pipeline](https://gitlab.com/jonascavalcantineto/tw-todobackend/pipelines)
