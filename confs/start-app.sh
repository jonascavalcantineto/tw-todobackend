#!/bin/bash

if [ -f ${APP_PATH}/target/${JAR_FILE} ]
then
    java -jar ${APP_PATH}/target/${JAR_FILE}
else
    echo "Package ${JAR_FILE} does not exist!!"
fi